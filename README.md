# mta-sts

Simple Mail Transfer Protocol (SMTP) Mail Transfer Agent (MTA) Strict Transport Security (STS). The purpose of MTA-STS is to encrypt and secure communications between SMTP servers via TLS (Transport Layer Security) preventing man-in-the-middle attackers from viewing and manipulating in-transit emails.

Available on https://mta-sts.aqoba.fr/.well-known/mta-sts.txt

See : https://dmarcian.com/mta-sts/
